from django.shortcuts import render
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from .models import Contents

FORM = """
<p>
Introduce un valor:
<p>
<form action="" method="POST">
    Valor: <input type="text" name="valor">
    <br/><input type="submit" name="enviar">
</form>
"""


# Create your views here.

@csrf_exempt
def get_content(request, llave):
    if request.method == "POST":
        valor = request.POST['valor']
        c = Contents(clave=llave, valor=valor)
        c.save()
    elif request.method == "PUT":
        valor = request.body.decode('utf-8')
        try:
            contenido = Contents.objects.get(clave=llave)
            contenido.delete()
        except Contents.DoesNotExist:
            pass
        c = Contents(clave=llave, valor=valor)
        c.save()
    try:
        contenido = Contents.objects.get(clave=llave)
        respuesta = contenido.valor
    except Contents.DoesNotExist:
        respuesta = FORM
    return HttpResponse(respuesta)
